import Vue from "vue/dist/vue.esm.browser";
import '../sass/app.sass'

const tagsArr = [
    { name: 'TRAVEL', color_class: 'tag_purple' },
    { name: 'EXPLORERS', color_class: 'tag_orange' },
    { name: 'NATURE', color_class: 'tag_green' },
    { name: 'СЕЛЬСКОЕ ХОЯЙСТВО', color_class: 'tag_green' },
    { name: 'КОРМА', color_class: 'tag_orange' },
    { name: 'SCIENCE', color_class: 'tag_blue' }
]

const cardsArr = [
    { tags: [ tagsArr[0] ], image: '/img/preview/1.jpg', header: 'Robot’s Penguin Disg', description: 'Figuring out how our brains work is key to understanding', subdesc: '' },
    { tags: [ tagsArr[1] ], image: '', header: '5 Things We Learned From X-Men: Days of Future Past', description: 'Peter Dinklage’s porn ‘tache, Jennifer Lawrence’s brilliance and more. Some (tiny) spoilers ahead', subdesc: '' },
    { tags: [ tagsArr[5] ], image: '', header: '14 Things Men Should Never Wear After 30', description: 'Light a bonfire in the garden, and step bravely into your best-dressed decade', subdesc: '' },
    { tags: [ tagsArr[3], tagsArr[4] ], image: '', header: 'Комбикорма для уток и свиней оптом', description: 'Наши корма дают +20% к приросту массы в год', subdesc: 'от 300 рублей/тонна' },
    { tags: [ tagsArr[1] ], image: '/img/preview/2.jpg', header: 'Stunning Changes Along Colorado River', description: 'Lake Powell offering kayakers new channels to explore', subdesc: '' },
    { tags: [ tagsArr[0] ], image: '', header: '14 Things Men Should Never Wear After 30', description: 'Light a bonfire in the garden, and step bravely into your best-dressed decade', subdesc: '' },
    { tags: [ tagsArr[2] ], image: '/img/preview/3.jpg', header: 'Offbeat Portraits Give Stars a New Turn', description: 'I whipped the Scotch tape from my pocket and said', subdesc: '' },
    { tags: [ tagsArr[1] ], image: '/img/preview/4.jpg', header: 'Must See Places of the New Year', description: 'Whether it’s India’s literary hub or mountain majesty', subdesc: '' },
    { tags: [ tagsArr[5] ], image: '/img/preview/5.jpg', header: 'Robot’s Penguin Disg', description: 'Figuring out how our brains work is key to understanding', subdesc: '' },
    { tags: [ tagsArr[0] ], image: '/img/preview/6.jpg', header: 'Stunning Video Reveals Invisible CO2', description: 'A NASA visualization shows the swirling gas', subdesc: '' },
]

new Vue({
    el: '#root',

    data: {
        tags: tagsArr,
        cards: cardsArr,
        selectedTags: [],
    },

    computed: {
        activeCards() {
            if(this.selectedTags.length == 0) return this.cards;

            let activeCards = [];
            let filters = this.selectedTags;

            this.cards.forEach(function(card) {
                card.tags.forEach(function (tag) {
                    if (filters.includes(tag.name) && !activeCards.includes(card)) {
                        activeCards.push(card);
                    }
                })
            });

            return activeCards;
        }
    }
});